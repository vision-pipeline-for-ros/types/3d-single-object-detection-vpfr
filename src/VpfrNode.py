#!/usr/bin/env python

"""
vpfr-3d-single-object-detection
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
from sensor_msgs.msg import Image
from vpfr3dsingleobjectdetection.msg import Single3DObjekt

from vpfr import VisionPipeline

if __name__ == "__main__":

    if len(sys.argv) < 4:
        print("usage: VpfrNode.py subscribechannel publishchannel")
    else:
        VisionPipeline(
            "vpfr3dsingleobjectdetection",
            sys.argv[1],
            Image,
            sys.argv[2],
            Single3DObjekt,
        )
